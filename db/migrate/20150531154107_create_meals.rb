class CreateMeals < ActiveRecord::Migration
  def change
    create_table :meals do |t|
      t.datetime :scheduled_date
      t.integer  :cost

      t.timestamps null: false
    end
  end
end
