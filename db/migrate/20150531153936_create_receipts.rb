class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.integer  :amount
      t.string   :merchant
      t.datetime :purchased_at

      t.timestamps null: false
    end
  end
end
