class Item < ActiveRecord::Base
  belongs_to :ingredient
  belongs_to :receipt

  def new
    @item = Item.new
  end

  def notified
    if expiration_date <= 3.days.from_expiration_date
      #generate email when expiration_date <= 3 days,
      #and give its value to notified_at
    else
      notified_at = nil
    end
  end

  def serialize
   #quantity is a changing number based on actual amount of item
   #bought is the initial quantity
   #remaining = quantity if any is taken away
   #reserved = generates notification based on a reserved amount
   
  end

end
